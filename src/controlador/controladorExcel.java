/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import java.awt.event.ActionEvent;
import java.awt.event.AWTEventListener;
import java.io.*;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import Negocio.SopaDeLetras;
import Vista.vistaPrincipal;
/**
 *
 * @author jefferson
 */
public class controladorExcel implements ActionListener{
    
    SopaDeLetras ModeloEX=new SopaDeLetras();
    vistaPrincipal VistaEX=new vistaPrincipal();
    JFileChooser SelectArchivo=new JFileChooser();
    File archivo;
    int contador=0;
    
    

    public controladorExcel(vistaPrincipal VistaEX, SopaDeLetras ModeloEX){
        this.VistaEX=VistaEX;
        this.ModeloEX=ModeloEX;
          //JTextArea dato=VistaEX.datosT;
        //SI LES APARECE LA LINEA ROJA COMO EN ESTE CASO
        //CLIC EN GUARDAR PARA ACTUALIZAR
        // O REPETIR LOS MISMOS PASOS ANTERIORES PUBLIC
        this.VistaEX.btnImportar.addActionListener(this);
        //this.VistaEX.jButton2.addActionListener(this);
        
        VistaEX.setVisible(true);
        VistaEX.setLocationRelativeTo(null);
    }
     public void AgregarFiltro(){
        SelectArchivo.setFileFilter(new FileNameExtensionFilter("Excel (*.xls)","xls"));
        SelectArchivo.setFileFilter(new FileNameExtensionFilter("Excel (*.xlsx)","xlsx"));
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        contador++;
        if(contador==1)AgregarFiltro();
        
        if(ae.getSource()==VistaEX.btnImportar){
            //VistaEX.jtDatos.getTableHeader().setVisible(false);
            if(SelectArchivo.showDialog(null, "Seleccionar Archivo Excel xls o xlsx")==JFileChooser.APPROVE_OPTION){
                archivo=SelectArchivo.getSelectedFile();
                //ALT + 124 ||
                if(archivo.getName().endsWith("xls")||archivo.getName().endsWith("xlsx")){
                  
                    JOptionPane.showMessageDialog(null, ModeloEX.Importar(archivo,VistaEX.jtDatos));
                }else{
                    JOptionPane.showMessageDialog(null, "Seleccionar formato Valido");
                }
            }
        }
    
    }}
    
    

